using System;
using RogueSharp;
using RogueSharp.MapCreation;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RLNET;

namespace Rug
{

    public static class PointExt
    {
        public static float Distance(this Point self, Point other)
        {
            Point t = self.Minus(other);
            return (float)Math.Sqrt (t.X * t.X + t.Y * t.Y);
        }

        public static Point Minus(this Point a, Point b)
        {
            return new Point (a.X - b.X, a.Y - b.Y);
        }
    }
}
