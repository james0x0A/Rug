﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RLNET;
using RogueSharp;
using RogueSharp.MapCreation;
using RogueSharp.Random;
using RSRand = RogueSharp.Random.Singleton;
namespace Rug
{
    internal class MainClass
    {
        private const int viewDistance = 16;
        private static int playerX = 5;
        private static int playerY = 5;
        private static RLRootConsole rootConsole;
        private static IMap map;
        private static bool loading;

        private static readonly RLCell playerCell = new RLCell( RLColor.Black, RLColor.LightBlue, '@' );

        public static Task<IMap> task;
        private static bool showMap;


        public static void Main( string[] args )
        {
            RLSettings settings = new RLSettings
            {
                BitmapFile = "ascii_8x8.png",
                CharWidth = 8,
                CharHeight = 8,
                Width = 16 * 8,
                Height = 9 * 8,
                Scale = 2f,
                Title = "Samplelike",
                WindowBorder = RLWindowBorder.Hidden,
                ResizeType = RLResizeType.None,
                StartWindowState = RLWindowState.Fullscreen
            };

            rootConsole = new RLRootConsole( settings );
            rootConsole.Update += rootConsole_Update;
            rootConsole.Render += rootConsole_Render;
            rootConsole.OnLoad += rootConsole_OnLoad;

            rootConsole.Run();
        }

        private static IMap MapFunc()
        {
            DotNetRandom r = RSRand.DefaultRandom;
            IMapCreationStrategy<Map> cm =
                new CaveMapCreationStrategy<Map>( rootConsole.Width, rootConsole.Height, 36, 16, 12 );
            IMap taskMap = cm.CreateMap();
            while ( !taskMap.GetCell( playerX, playerY ).IsWalkable )
            {
                playerX = r.Next( taskMap.Width - 1 );
                if ( !taskMap.GetCell( playerX, playerY ).IsWalkable )
                {
                    playerY = r.Next( taskMap.Height - 1 );
                }
            }
            return taskMap;
        }

        private static void rootConsole_OnLoad( object sender, EventArgs e )
        {
            Func<IMap> func = MapFunc;
            task = new Task<IMap>( func );
            loading = true;
            task.Start();
        }

        private static void rootConsole_Update( object sender, UpdateEventArgs e )
        {
            if ( loading )
            {
                if ( task.IsCompleted )
                {
                    map = task.Result;
                    loading = false;
                }
                else
                {
                    return;
                }
            }

            RLKeyPress keyPress = rootConsole.Keyboard.GetKeyPress();
            if ( keyPress != null )
            {
                int tempY = playerY;
                int tempX = playerX;
                if ( keyPress.Key == RLKey.Up )
                {
                    tempY--;
                }
                else if ( keyPress.Key == RLKey.Down )
                {
                    tempY++;
                }
                else if ( keyPress.Key == RLKey.Left )
                {
                    tempX--;
                }
                else if ( keyPress.Key == RLKey.Right )
                {
                    tempX++;
                }
                if ( keyPress.Key == RLKey.Escape )
                {
                    rootConsole.Close();
                }
                if ( keyPress.Key == RLKey.C )
                {
                    showMap = false;
                }
                if ( keyPress.Key == RLKey.V )
                {
                    showMap = true;
                }

                if ( map.GetCell( tempX, tempY ).IsWalkable )
                {
                    playerX = tempX;
                    playerY = tempY;
                }
            }
            map.ComputeFov( playerX, playerY, viewDistance, true );
            IEnumerable<Cell> cells = map.GetCellsInRadius( playerX, playerY, viewDistance + 2 );
            foreach ( Cell cell in cells )
            {
                if ( cell.IsInFov )
                {
                    map.SetCellProperties( cell.X, cell.Y, cell.IsTransparent, cell.IsWalkable, true );
                }
            }
        }

        private static void rootConsole_Render( object sender, UpdateEventArgs e )
        {
            Point p = new Point( playerX, playerY );
            rootConsole.Clear();
            if ( loading )
            {
                rootConsole.Print( 0, 0, "Loading...", RLColor.LightBlue, RLColor.Black );
                rootConsole.Draw();
                return;
            }
            for ( int x = 0 ; x < rootConsole.Width ; x++ )
            {
                for ( int y = 0 ; y < rootConsole.Height ; y++ )
                {
                    Cell cell = map.GetCell( x, y );
                    int ch = cell.IsExplored || showMap ? cell.ToString().ToCharArray()[0] : ' ';
                    rootConsole.SetChar( x, y, ch );
                    if ( cell.IsInFov )
                    {
                        Point c = new Point( cell.X, cell.Y );
                        float cellDist = p.Distance( c );
                        float f = viewDistance / ( viewDistance - cellDist );
                        float lightLv = 1 / ( f * f );
                        RLColor orange = new RLColor( 1f, 0.647058845f, 0 );
                        rootConsole.SetBackColor( cell.X, cell.Y, orange * lightLv );
                        rootConsole.SetColor( cell.X, cell.Y, RLColor.Gray + RLColor.White * lightLv );
                    }
                    else
                    {
                        rootConsole.SetColor( cell.X, cell.Y, RLColor.Gray );
                    }
                }
            }


            rootConsole.Set( playerX, playerY, playerCell );

            int color = 1;
            if ( rootConsole.Mouse.LeftPressed )
            {
                color = 4;
            }
            rootConsole.SetBackColor( rootConsole.Mouse.X, rootConsole.Mouse.Y, RLColor.CGA[color] );

            rootConsole.Draw();
        }
    }
}